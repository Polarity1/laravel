<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('test');
});

Route::get('shopping-cart', function () {
    return view('shopping-cart');
});

Route::get('checkout', function () {
    return view('chekout');
});

Route::get('/add-to-cart/{id}' , ['uses' => 'ProductController@addToCart', 'as' => 'product.addToCart']);

Route::get('/reduce/{id}' , ['uses' => 'ProductController@reduceByOne', 'as' => 'product.reduceByOne']);

Route::get('/remove/{id}' , ['uses' => 'ProductController@removeItem', 'as' => 'product.removeItem']);

Route::get('/shopping-cart' , ['uses' => 'ProductController@getCart', 'as' => 'product.shoppingCart']);

Route::get('/checkout' , ['uses' => 'ProductController@getCheckout', 'as' => 'checkout']);

Route::post('/checkout' , ['uses' => 'ProductController@postCheckout', 'as' => 'checkout']);


Route::group(['middleware' => 'auth'], function () {

    Route::get('/profile', ['uses' => 'UserController@getProfile', 'as' => 'user.profile']);

    Route::get('users/{user}',  ['as' => 'users.edit', 'uses' => 'UserController@edit']);

    Route::patch('users/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update']);

    Route::patch('shipping/update',  ['as' => 'shipping.update', 'uses' => 'ShippingController@update']);

    Route::get('image-upload', 'ImageUploadController@imageUpload')->name('image.upload');

    Route::post('image-upload', 'ImageUploadController@imageUploadPost')->name('image.upload.post');

});


Route::group(['middleware' => 'admin'], function () {

    Route::get('/admin', function() {
        return view('admin-start');
    });

    Route::get('/admin-add-product', function () {
        return view('admin-add-product');
    });

    Route::get('/admin-add-user', function () {
        return view('admin-add-user');
    }); 

    Route::get('/admin-user', ['uses' => 'AdminController@users', 'as' => 'admin.users']);

    Route::get('/admin-products', ['uses' => 'AdminController@products', 'as' => 'admin.products']);

    Route::get('/admin-user-detail/{id}',  ['uses' => 'AdminController@userDetail', 'as' => 'admin.user.detail']);

    Route::get('/admin-user-delete/{id}',  ['uses' => 'AdminController@userDelete', 'as' => 'admin.user.delete']);

    Route::patch('/admin-add-product/add', ['uses' => 'AdminController@addProduct', 'as' => 'admin.add.product']);

    Route::patch('/admin-add-user/add', ['uses' => 'AdminController@addUser', 'as' => 'admin.add.user']);

    Route::get('/admin-product-delete/{id}',  ['uses' => 'AdminController@productDelete', 'as' => 'admin.delete.product']);
    
    Route::get('/admin-product-detail/{id}',  ['uses' => 'AdminController@productDetail', 'as' => 'admin.product.detail']);
    
    Route::patch('/admin-product-detail/{id}/edit',  ['uses' => 'AdminController@productEdit', 'as' => 'admin.edit.product']);

    Route::get('/admin-orders',  ['uses' => 'AdminController@getOrders', 'as' => 'admin.orders']);

    Route::get('/admin-order-detail/{id}',  ['uses' => 'AdminController@orderDetail', 'as' => 'admin.order.detail']);

    Route::get('/admin-finance',  ['uses' => 'AdminController@getOrders', 'as' => 'admin.timedOrders']);

    Route::get('/admin-finance/{time}',  ['uses' => 'AdminController@getTimedOrders', 'as' => 'admin.timeOrders']);

    Route::patch('/admin-user-detail/{id}/edit',  ['uses' => 'AdminController@editShipping', 'as' => 'admin.edit.shipping']);

});


Route::get('/', [ 'uses' => 'ProductController@getIndex', 'as' => 'product.index']);

Route::redirect('/home', '/');

Auth::routes();

Route::get('/home', [ 'uses' => 'ProductController@getIndex', 'as' => 'product.index']);

Auth::routes();

Route::get('/home', 'ProductController@getIndex')->name('home');
