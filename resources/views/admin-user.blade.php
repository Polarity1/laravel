@extends('admin')

@section('content')
<div class="container">
        <div class="col-sm-5 col-md-4 offset-sm-6 offset-md-6 offset-lg-2">
    <table id="users">
        <tr>
            <th>ID:</th><th>Name:</th><th>Email:</th><th>Orders:</th><th>Delete:</th>
        </tr>
        @foreach ($users as $user)
            <tr style="border-bottom: 1px solid rgba(0,0,0,.125);">
               <td><a href="admin-user-detail/{{ $user->id }}">{{ $user->id }}</a></td><td id="long">{{ $user->name }}</td><td>{{ $user->email }}</td><td>{{ count($user->orders) }}</td>
               <td>
                @if (Auth::user()->id == $user->id)
                    <a href="#"></a>
                @else
                    <a href="/admin-user-delete/{{ $user->id }}">X</a>
                @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>
</div>
@endsection