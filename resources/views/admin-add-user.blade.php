@extends('admin')

@section('content')
<?php $error = null; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <strong>Add User</strong>
                </div>

                <div style="margin: 5px;" class="card-body">

                    <form method="post" action="{{ route('admin.add.user') }}">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}

                    <table style="margin-top: 4px;">
                        <tr>
                            <td>User Name:</td><td><input type="text" name="name" placeholder="User Name" required></td>
                        </tr>
                        <tr>
                            <td>User Email:</td><td><input type="email" name="email" placeholder="example@woop.com" required></td>
                        </tr>
                        <tr>
                            <td>User Password:</td><td><input type="password" name="password" required></td>
                        </tr>
                        <tr>
                            <td>Confirm Password:</td><td><input type="password" name="password_confirmation" required></td>
                        </tr>
                    </table>
                    <button style="margin-top: 3px" type="submit">Add User</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
