@extends('admin')

@section('content')
<div class="container">
    <div class="col-sm-10 col-md-10 col-lg-10 offset-sm-6 offset-md-6 offset-lg-2">
        
        <div style="display: none">
                <?php $x = 0; ?>
                @foreach ($dailyOrders as $tt)
                <?php $x++; $totalP = 0; ?>
                dag <span class="days">{{ $x }}</span> orders: {{ count($tt) }}
                        @foreach($tt as $ttt)
                            @if (!$ttt->cart->totalPrice)
                                {{ 'No Items' }}
                            @else
                                {{ $ttt->cart->totalPrice }}
                                <?php $totalP += $ttt->cart->totalPrice; ?>
                            @endif
                        @endforeach
                        total: <span class="tp">{{ $totalP }}</span>
                        <br>
                @endforeach
        </div>
        <div class="container">
             <div class="pull-left">
                   
                <select name="lookBack" onchange="selectChange();">
                    <option value="1" @if ($time == 1) {{ 'selected' }} @endif>1 day</option>
                    <option value="2" @if ($time == 2) {{ 'selected' }} @endif>2 days</option>
                    <option value="3" @if ($time == 3) {{ 'selected' }} @endif>3 days</option>
                    <option value="4" @if ($time == 4) {{ 'selected' }} @endif>4 days</option>
                    <option value="5" @if ($time == 5) {{ 'selected' }} @endif>5 days</option>
                    <option value="6" @if ($time == 6) {{ 'selected' }} @endif>6 days</option>
                    <option value="7" @if ($time == 7) {{ 'selected' }} @endif>7 days</option>
                    <option value="14" @if ($time == 14) {{ 'selected' }} @endif>2 weeks</option>
                    <option value="30" @if ($time == 30) {{ 'selected' }} @endif>1 month</option>
                    <option value="93" @if ($time == 93) {{ 'selected' }} @endif>3 month</option>
                    <option value="178" @if ($time == 178) {{ 'selected' }} @endif>6 month</option>
                    <option value="365" @if ($time == 365) {{ 'selected' }} @endif>1 year</option>
                </select>
            </div>
        <table id="orders">
            <tr>
                <th>ID:</th><th>Customer:</th><th>Price (&euro;):</th><th>Placed:</th>
            </tr>
            @foreach ($orders as $order)
            
            <tr style="border-bottom: 1px solid rgba(0,0,0,.125);">
                <td>{{ $order->payment_id }}</td><td id="long">{{ App\User::find($order->user_id)->email }}</td><td>{{ number_format($order->cart->totalPrice,2) }}</td><td>{{ $order->created_at }}</td>
            </tr>
            @endforeach
        </table>
    </div>
    <div>Total Revenue (€):<span id="revenue"> {{ $total }}</span><span class="pull-right">Time Frame: <span id="timeFrame"></span> day(s)</span></div>
    <div>Average revenue per day (€): <span id="revPerDay"></span></div>
    <div id="chart_div"></div>
</div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    var temp = String(window.location);
    var passedTime = temp.split('/');
    var passedDays = parseFloat(passedTime[passedTime.length -1]);
    document.getElementById('timeFrame').innerHTML = passedDays;
    var rev = parseFloat(document.getElementById('revenue').innerHTML);
    var rPD = rev / passedDays;
    var revPD = rPD.toFixed(2);
    var rawData = [];
    var i;
    var Arr = document.getElementsByClassName("tp");
        for (i = 0; i < Arr.length; i++) { 
            console.log(Arr[i].innerHTML);
            rawData.push(Arr[i].innerHTML);
        }
    console.log(rawData);
    document.getElementById('revPerDay').innerHTML = parseFloat(revPD);
    google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

    let datalist = [];
     for (let i in rawData) {
        var A = [parseFloat(i), parseFloat(rawData[i])];
         datalist.push(A);
     }
     var A = [rawData.length, parseFloat(rawData[rawData.length -1])];
     datalist.push(A);
    console.log(datalist);

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Euro');
      data.addColumn('number', 'Sales');

      data.addRows(
       datalist
      );

      var options = {
        hAxis: {
          title: 'Days'
        },
        vAxis: {
          title: 'Revenue (€)'
        }
      };
      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }

    function selectChange() {
        window.location.href = "http://homestead.test/admin-finance/" + event.target.value;
    }
    </script>
@endsection