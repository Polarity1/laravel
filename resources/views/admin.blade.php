@extends('layouts.app')

<div class="container-bar">
    <div class="leftside-bar">
            <a href="/admin">Admin Panel</a>
        <hr>
            <br>
            <a href="{{ route('admin.users') }}">User Management</a><br>
            <a href="/admin-add-user">Add User</a><br>      
        <hr>
            <a href="{{ route('admin.products') }}">Products</a><br>
            <a href="/admin-add-product">Add Products</a><br>
        <hr>
            <a href="/admin-orders">Orders Overview</a><br>
            <a href="/admin-finance/7">Finances</a><br>
    </div>
</div>