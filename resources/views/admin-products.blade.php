@extends('admin')

@section('content')
<div class="container">
        <div class="col-sm-5 col-md-3 offset-sm-6 offset-md-6 offset-lg-3">
    <table id="products">
        <tr>
            <th>ID:</th><th>Name:</th><th>Price (&euro;):</th><th>Delete:</th>
        </tr>
        @foreach ($products as $product)
        <tr style="border-bottom: 1px solid rgba(0,0,0,.125);">
            <td><a href="/admin-product-detail/{{ $product->id }}">{{ $product->id }}</a></td><td id="long">{{ $product->title }}</td><td>{{ number_format($product->price,2) }}</td><td><a href="/admin-product-delete/{{ $product->id }}">X</a></td>
        </tr>
        @endforeach
    </table>
</div>
</div>
@endsection