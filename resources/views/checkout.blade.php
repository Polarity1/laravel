@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>Checkout</strong>
                    <div class="pull-right">
                        Total Price: {{ Auth::user()->currency }}<strong>{{ number_format($total * $c_factor,2) }}</strong>
                    </div
                ></div>
   
                    <div class="row">
                        <div class="col-sm-10 col-md-10 col-md-offset-4 col-sm-offset-4">
                        <div id="charge-error" class="alert alert-danger {{ Session::has('error') ? 'hidden' : '' }}">
                            {{ Session::get('error') }}
                        </div>
                        <form action="{{ route('checkout') }}" method="post" id="checkout-form">
                            
                                <div class="col-xs-12 col-xs-offset-3">
                                    <div style="margin-left: 20px;" class="form-group">
                                        <label for="card-name">Card Holder Name</label>
                                        <input type="text" id="card-name" class="form-control" required>

                                        <label for="card-number">Card Number</label>
                                        <input type="number" id="card-number" class="form-control" required>
                                        <div style="margin-left: 0px;" class="row">
                                        <div class="col-xs-6 col-xs-offset-3">
                                        <label for="card-expiry-month">Card Exp. Month</label>
                                        <input type="text" id="card-expiry-month" class="form-control" required>
                                        </div>
                                        <div class="col-xs-6 col-xs-offset-3" style="margin-left: 20px;">
                                        <label for="card-expiry-year">Card Exp. Year</label>
                                        <input type="text" id="card-expiry-year" class="form-control" required>
                                        </div>
                                        </div>
                                        <label for="card-cvc">Card CVC</label>
                                        <input type="text" id="card-cvc" class="form-control" required>

                                    </div>
                                </div>
                          {{ csrf_field() }}
                          <button type="submit" class="btn btn-success">Buy now!</button>
                        </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://js.stripe.com/v2/"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ URL::to('js/checkout.js') }}"></script>
@endsection