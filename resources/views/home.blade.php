@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @guest
                <div class="card-header"> Welcome <strong>Stranger</strong>! </div>
                @endguest
                @auth
                <div class="card-header"> Welcome <strong>{{ Auth::user()->name }}</strong>! </div>
                @endauth
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table>
                    <tr>
                        <td>Current date:</td><td><?php echo date("d F Y") ?></td>
                    </tr>
                    <tr>
                        <td>Current time:</td><td><?php 
                        date_default_timezone_set("Europe/Amsterdam");
                        echo date("H:i:s"); ?></td>
                    </tr>
                    </table>


                </div>
                <div style="border-top: 1px solid rgba(0, 0, 0, .125)" class="card-header"> 
                <strong>Product overview</strong>
                </div>
                <div class="card-body">
                    @if (Session::has('success'))
                    <div id="charge-message" class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                    @endif
                    @foreach ($products->chunk(3) as $productChunk)
                    <div class="row">
                        @foreach ($productChunk as $product)
                        <div class="col-sm-6 col-md-3 col-border">
                            <div class="thumbnail">
                                <img src="{{ $product->imagePath }}" style="max-height: 110px" class="img-responsive">
                                    <div class="caption">
                                    <h3>{{ $product->title }}</h3>
                                    <p style="min-height: 75px;" class="description">{{ $product->description }}</p>
                                        <div class="clearfix">
                                            <div class="pull-left price">
                                            @Auth
                                            {{ Auth::user()->currency }}{{ number_format($product->price * $c_factor,2) }}
                                            @endauth
                                            @guest
                                            €{{ $product->price }}
                                            @endguest
                                            </div>
                                            <a href="add-to-cart/{{ $product->id }}" class="btn btn-success pull-right" role="button">Add to Cart</a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
