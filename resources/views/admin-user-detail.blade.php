@extends('admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <a href="{{ route('admin.users') }}" class="btn btn-default">Back</a>

                <div class="card-header"> <strong>{{ $user->name }}</strong>
                    <div style="float:right"> 
                    @if ($user->updated_at)
                        <strong>Last updated:</strong>&emsp;{{ $user->updated_at }}
                    @endif
                    </div>
                </div>

                <div style="margin: 5px;" class="card-body">
                    
                    <form method="post" action="{{ route('users.update', $user) }}">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}

                    <table style="margin-top: 4px;">
                        <tr>
                            <td>ID:</td><td>{{ $user->id }}</td>
                        </tr>
                        <tr>
                            <td>Created:</td><td>{{ $user->created_at }}</td>
                        </tr>
                        <tr>
                            <td>Name:</td><td> <input type="text" name="name"  value="{{ $user->name }}" /></td>
                        </tr>
                        <tr>
                            <td>Email:</td><td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <td>Currency:</td><td>
                                <select name="currency">
                                    <option value="€" <?php if($user->currency == "€") { echo 'selected'; } ?>>€</option>
                                    <option value="$" <?php if($user->currency == "$") { echo 'selected'; } ?>>$</option>
                                    <option value="£" <?php if($user->currency == "£") { echo 'selected'; } ?>>£</option>
                                </select>
                        </tr>
                        <tr>
                            <td>Password:</td><td><input type="password" name="password" /></td>
                        </tr>
                        <tr>
                            <td>Confirm Password:</td><td><input type="password" name="password_confirmation" /></td>
                        </tr>
                        <tr>
                            <td>Make Admin:</td><td><input type="checkbox" name="admin" <?php if($user->role_id == 1) { echo 'checked'; } ?>></td>
                        </tr>
                    </table>
                    <button style="margin-top: 3px" type="submit">Update Profile</button>
                    </form>
                </div>
                <div class="shipping">
                <div style="border-top: 1px solid rgba(0, 0, 0, .125);" onclick="Collapse();" class="card-header test">
                    <strong>Shipping details</strong>
                </div>
                <div class="details">
                    <?php
                     $shipping = DB::table('shipping')->where('user_id', $user->id )->first();
                     if (empty($shipping)) {
                        $shipping = (object) [
                        'last_name' => '',
                        'first_name' => '',
                        'adress' => '',
                        'postal_code' => '',
                        'city' => '',
                        'country' => ''
                        ];
                     }
                     ?>
                    <form method="post" action="{{ route('admin.edit.shipping', $user->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('patch') }}

                    <table style="margin-top: 14px; margin-left: 20px;">
                        <tr>
                            <td>First Name:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td><td><input type="text" name="first_name" value="{{ $shipping->first_name }}"></td>
                        </tr>
                        <tr>
                            <td>Last Name:</td><td><input type="text" name="last_name" value="{{ $shipping->last_name }}"></td>
                        </tr>
                        <tr>
                            <td>Address:</td><td><input type="text" name="adress"  value="{{ $shipping->adress }}" /></td>
                        </tr>
                        <tr>
                            <td>Postal Code:</td><td><input type="text" name="postal_code" value="{{ $shipping->postal_code }}"></td>
                        </tr>
                        <tr>
                            <td>City/Town:</td><td><input type="text" name="city" value="{{ $shipping->city }}" /></td>
                        </tr>
                        <tr>
                            <td>Country:</td><td><input type="text" name="country" value="{{ $shipping->country }}" /></td>
                        </tr>
                    </table>
                    <button style="margin: 3px; margin-left: 20px; margin-bottom: 20px;" type="submit">Update Shipping Details</button>
                    </form>
                </div>
                </div>
                <div style="border-top: 1px solid rgba(0, 0, 0, .125);" onclick="Collapse();" class="card-header test">
                    <strong>Orders</strong>
                </div>

                
                    <div class="details">
                     @foreach ($orders as $order)
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                        Order: <strong>{{ $order->payment_id }}</strong>
                          <ul class="list-group text-left">
                              @foreach($order->cart->items as $item)
                              <li class="list-group-item">
                                    <span class="badge">
                                            {{ $order->currency }}{{ $item['price'] }}
                                    </span>
                                            {{ $item['item']['title'] }} 
                                    <span class="pull-right">      
                                            {{ $item['qty'] }}
                                    </span>
                              </li>
                              @endforeach
                          </ul>
                        </div>
                        <div style="margin-left: 20px;" class="panel-footer">
                            <strong>Total: {{ $order->currency }}{{ $order->cart->totalPrice }}</strong>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
var content;
function Collapse() {
    if (event.target.tagName == "STRONG") {
        event.target.parentNode.classList.toggle("active");
        content = event.target.parentNode.nextElementSibling;
        Change();
    } else {
        event.target.classList.toggle("active");
        content = event.target.nextElementSibling;
        Change();
    }
}

function Change() {
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }     
}
</script>             
@endsection