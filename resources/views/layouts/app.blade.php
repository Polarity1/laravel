<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Barts Project</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    
<!--
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" integrity="sha384-PmY9l28YgO4JwMKbTvgaS7XNZJ30MK9FAZjjzXtlqyZCqBY6X6bXIkM++IkyinN+" crossorigin="anonymous">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap-theme.min.css" integrity="sha384-jzngWsPS6op3fgRCDTESqrEJwRKck+CILhJVO5VvaAZCq8JYf8HsR/HPpBOOPZfR" crossorigin="anonymous">


    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" integrity="sha384-vhJnz1OVIdLktyixHY4Uk3OHEwdQqPppqYR8+5mjsauETgLOcEynD9oPHhhz18Nw" crossorigin="anonymous"></script>
    -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            font-size: 1rem;
        }
        .thumbnail img {
            text-align: center;
            margin-bottom: 4px;
        }

        .pull-right {
            float: right;
        }
        .pull-left {
            float: left;
        }
        .col-border {
            border: 1px solid rgba(0,0,0,.125);
            padding: 5px;
            margin-left: 27px;
            margin-right: 27px;
            margin-top: 5px;
            margin-bottom: 5px;

        }

        .price {
            font-weight: bold;
            font-size: 1rem;
        }

        .thumbnail .description {
            color: #7f7f7f;
            font-size: 0.9rem;
        }

        .test {
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
        }

        .active, .test:hover {
            background-color: #777;
        }

        .details {
            padding: 0 18px;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
        }
        .dropdown-menu-left {
            width: 50px;
        }

        .col-border {
             left: 85px;
             margin-left: 10px;
        }


        .container-bar {
            top: 57px;
            position: fixed;
            text-align: center;
            height: 100%;
            width: 150px;
            border-right: 1px solid rgba(0,0,0,.125);
            margin-right: 15px;
        }
        .container-bar .leftside-bar {
            padding: 5px;
            margin-top: 7px;
        }

        table#users {
            width: 750px;
            text-align: center;
        }

        table#products {
            width: 550px;
            text-align: center;
        }
        
        table#orders {
            width: 775px;
            text-align: center;
        }

        textarea {
            border: 1px solid rgba(0,0,0,.125);
        }
        </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="collapse navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                                    Currency: &emsp;
                                    @Auth
                                    {{ Auth::user()->currency }} <span style="width: 20px;" class="caret"></span>
                                    @endAuth
                                    @guest
                                    € <span style="width: 20px;" class="caret"></span>
                                    @endguest
    
                    </ul>
            </div>
            <div class="container">
                <a class="navbar-brand" href="http://homestead.test">
                    Home
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @endguest
                        
                        @Auth
                            @if (!Auth::user()->avatar)
                                <img src="images/stock.png" style="height: 43px; margin-right: 5px; margin-top: -4px; border-radius: 23px;" alt="{{ Auth::user()->name }}">
                            @else 
                                <img src="/images/{{ Auth::user()->avatar }}" style="height: 43px; margin-right: 5px; margin-top: -4px; border-radius: 23px;" alt="{{ Auth::user()->name }}">
                            @endif

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="http://homestead.test/">
                                        {{ __('Home') }}
                                    </a>
                                    <a class="dropdown-item" href="http://homestead.test/test">
                                        {{ __('About') }}
                                    </a>
                                    <a class="dropdown-item" href="http://homestead.test/profile">
                                        {{ __('User Profile') }}
                                    </a>
                                    <a class="dropdown-item" href="http://homestead.test/shopping-cart">
                                        {{ __('Basket') }} <span class="badge"> {{ Session::has('cart') ? Session::get('cart')->totalQty : ' ' }}</span>
                                    </a>
                                    <?php
                                    if (Auth::user()->role_id == 1) {
                                        echo '<hr><a class="dropdown-item" href="http://homestead.test/admin">Admin</a>';
                                    }
                                    ?>
                                    <hr>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @yield('scripts')
</body>
</html>
