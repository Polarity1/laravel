@extends('layout')
@extends('layouts.app')

@section('title', "About me")

@section('name')
        Bart Brinks
@endsection

@section('content2')
        Junior Web Developer
@endsection

@section('skills')
{{ Cache::get('USD') }}
        <div class="skill">
                HTML/CSS<br>
                <progress value="100" max="100"></progress><br>
        </div>
        <div class="skill">
                JavaScript<br>
                <progress value="100" max="100"></progress><br>
        </div>
        <div class="skill">
                PHP<br> 
                <progress value="95" max="100">Native PHP</progress><br>
        </div>
@endsection