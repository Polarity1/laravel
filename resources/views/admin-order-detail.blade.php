@extends('admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                    <a href="{{ route('admin.orders') }}" class="btn btn-default">Back</a>

                <div class="card-header"> <strong>Order: {{ $order->payment_id }}</strong>
                </div>

                <div style="margin: 5px;" class="card-body">
    
                <ul class="list-group text-left">
                @foreach(unserialize($order->cart)->items as $item)
                    <li class="list-group-item">
                        <span class="badge">
                                {{ $order->currency }}{{ $item['price'] }}
                        </span>
                                {{ $item['item']['title'] }} 
                        <span class="pull-right">      
                                {{ $item['qty'] }}
                        </span>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div style="margin-left: 20px;" class="panel-footer">

                <strong>Total: {{ $order->currency }}{{ unserialize($order->cart)->totalPrice }}</strong>    
            </div>
        </div>
    </div>
</div>

@endsection