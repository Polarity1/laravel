@extends('admin')

@section('content')
<?php $error = null; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <strong>Add Product</strong>
                </div>

                <div style="margin: 5px;" class="card-body">

                    <form method="post" action="{{ route('admin.add.product') }}">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}

                    <table style="margin-top: 4px;">
                        <tr>
                            <td>Product Name:</td><td><input type="text" name="name" placeholder="Product Name" required></td>
                        </tr>
                        <tr>
                            <td>Image Path:</td><td><input type="text" name="imagePath" placeholder="http link to image" required></td>
                        </tr>
                        <tr>
                            <td>Description:</td><td><textarea rows="4" cols="20" name="description" placeholder="Product Description" required></textarea></td>
                        </tr>
                        <tr>
                            <td>Price (in EUR):</td><td><input type="number" min="0" max="100" step="0.01" name="price" required></td>
                        </tr>
                    </table>
                    <button style="margin-top: 3px" type="submit">Add Product</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
