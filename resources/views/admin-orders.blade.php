@extends('admin')

@section('content')
<div class="container">
        <div class="col-sm-10 col-md-10 col-lg-10 offset-sm-6 offset-md-6 offset-lg-2">
    <table id="orders">
        <tr>
            <th>ID:</th><th>Customer:</th><th>Price (&euro;):</th><th>Placed:</th><th>Delete:</th>
        </tr>
        @foreach ($orders as $order)
        
        <tr style="border-bottom: 1px solid rgba(0,0,0,.125);">
            <td><a href="/admin-order-detail/{{ $order->payment_id }}">{{ $order->payment_id }}</a></td><td id="long">{{ App\User::find($order->user_id)->email }}</td><td>{{ number_format($order->cart->totalPrice,2) }}</td><td>{{ $order->created_at }}</td><td><a href="#">X</a></td>
        </tr>
        @endforeach
    </table>
</div>
</div>
@endsection