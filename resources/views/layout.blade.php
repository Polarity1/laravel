<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <script src="main.js"></script>

 <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .subt {
                font-size: 27px;
                cursor: pointer;
                transform: scale(1);
                transition: ease-out;
                transition-duration: 0.7s;
            }

            .subt:hover {
                transform: scale(1.15);
                transition: ease-in-out;
                transition-duration: 0.7s;
            }

            .name { 
                margin-left: auto;
                margin-right: auto;   
                width: 500px;
                height: 110px;
                animation: bounceIn 0.6s;
                transform: rotate(0deg) scale(1) translateZ(0);
                transition: all 0.4s cubic-bezier(.8,1.8,.75,.75);
                cursor: pointer;
            }

            .name:hover {
                transform: rotate(8deg) scale(1.1);
            }

            @keyframes bounceIn {
                0% {
                    transform: scale(.3);
                } 
                50% {
                    transform: scale(1.05);
                } 
                70% {
                    transform: scale(.9);
                }
                100% {
                    transform: scale(1);
                }
            }  

            .sidebar {
                position: fixed;
                top: 5vh;
                width: 100px;
            } 
            a:hover {
                color: #ff0000;
                transition-duration: 0.5s;
            }

            progress {
                border-radius: 25px;
                margin: 5px;
                height: 20px;
                width: 50%;
                text-align: center;
                background-color: #fff;
            }

            .skill {
                margin-top: 5px;
            }

            td {
                margin: 10px;
            }
    </style>
</head>
<body>

<div class="content">
    <div class="title m-b-md">
        <div class="name">
        @yield('name')
        </div>
    </div>
    <div class="subt">
        @yield('content2')
    </div>
    <div class="progress">
    </div>
    @yield('skills')
</div>
</body>
</html>

<?php 

?>
