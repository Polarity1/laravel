@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Shopping Basket </div>
   
    @if (Session::has('cart') )
        <div class="row">
            <div class="col-sm-12 col-md-12 col-md-offset-3 col-sm-offset-3">
               <ul class="list-group">
                    @foreach ($products as $product)
                         <li class="list-group-item">
                            <span class="badge">{{ $product['qty'] }}</span>
                            <strong>{{ $product['item']['title'] }}</strong>
                            <span class="label label-success">{{ Auth::user()->currency }}{{ number_format($product['price'] * $c_factor,2) }}</span>
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle pull-right" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('product.reduceByOne', ['id' => $product['item']['id'] ]) }}">Reduce by 1</a></li>
                                    <li><a href="{{ route('product.removeItem', ['id' => $product['item']['id'] ]) }}">Reduce All</a></li>
                                </ul>
                            </div>
                         </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <strong>Total: {{ Auth::user()->currency }}{{ number_format( $totalPrice * $c_factor,2) }}</strong>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <a href="{{ route('checkout') }}" class="btn btn-success">Checkout</a>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <h2>No Items In Cart</h2>
            </div>
        </div>
    @endif
    </div>
    </div>
    </div>
    </div>
    </div>    
@endsection