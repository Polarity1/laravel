<?php
  
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
  
class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUpload()
    {
        return view('imageUpload');
    }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUploadPost()
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:1024',
        ]);
  
        $imageName = Auth::user()->id .'.'.request()->image->getClientOriginalExtension();
            
        request()->image->move(public_path('images'), $imageName);


        DB::table('users')
            ->where('id', Auth::user()->id )
            ->update(['avatar' => $imageName]);

        return back();
        
    }
}