<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\Http\Requests;
use Session;
use App\Cart;
use Stripe\Stripe;
use Stripe\Charge;
use Cache;
use Carbon\Carbon;

class ProductController extends Controller
{

    public function getIndex() {
        ProductController::getRates();
        $products = Product::all();
        $currency_factor = ProductController::currency();
        return view('home', ['products' => $products, 'c_factor' => $currency_factor]);
    }

    public function addToCart(Request $request, $id) {
        $product =  Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $id);

        $request->session()->put('cart', $cart);
        return redirect()->route('product.index');
    }

    public function reduceByOne($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);

        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        } 
        return redirect()->route('product.shoppingCart');
    }

    public function removeItem($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        } 
        return redirect()->route('product.shoppingCart');
    }

    public function getCart() {
        $currency_factor = ProductController::currency();
        if (!Session::has('cart')) {
            return view('shopping-cart');
        } 
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('shopping-cart', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'c_factor' => $currency_factor]);
    }

    public function getCheckout() {
        $currency_factor = ProductController::currency();
        if (!Session::has('cart')) {
            return view('shopping-cart');
        } 
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('checkout', ['total' => $total, 'c_factor' => $currency_factor]);
    }

    public function postCheckout(Request $request) {
        if (!Session::has('cart')) {
            return redirect()->route('shopping-cart');
        } 
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        $currency_factor = ProductController::currency();

        switch(Auth::user()->currency) {
            case '€':
            $currency = "eur";
            break;
            case '$':
            $currency = "usd";
            break;
            case '£':
            $currency = "gbp";
            break;
        }

        Stripe::setApiKey('sk_test_32Mg3dicXYI2PYz5FCBA8TvR');
        try {
            $charge = Charge::create(array(
                "customer" => "cus_EQrMwGAghx9vO0",
                "amount" => number_format($cart->totalPrice * $currency_factor,2) * 100,
                "currency" => $currency,
                "source" => $request->input('stripeToken'),
                "description" => "Example charge",
                'receipt_email' => Auth::user()->email
            ));
            $order = new Order();
            $order->cart = serialize($cart);
            $order->payment_id = $charge->id;
            $order->currency = Auth::user()->currency;
            Auth::user()->orders()->save($order);     
        } catch (\Exception $e) {
            return redirect()->route('checkout')->with('error', $e->getMessage());

        }
        Session::forget('cart');
        return redirect()->route('product.index')->with('success', 'Successfully purchased products');
    }

    public function currency() {
        if (Auth::user()) {
            switch(Auth::user()->currency) {
                case '€':
                $currency_factor = (double) Cache::get('EUR');
                break;
                case '$':
                $currency_factor = (double) Cache::get('USD');
                break;
                case '£':
                $currency_factor = (double) Cache::get('GBP');
                break;
            }
        } else {
            $currency_factor = (double) 1;
        }
        return $currency_factor;
    }

    public function getRates() {
        if (!Cache::has('USD')) {
            $getData = ProductController::callAPI('GET', 'http://data.fixer.io/api/latest?access_key=9f7a01183c709099578338d1b36aa589&symbols=USD,GBP,CAD,AUD,EUR&format=1', false);
            $response = json_decode($getData, true);
            foreach ($response['rates'] as $key => $rate) {
                Cache::put($key, $rate, 60);
            }
        }
    }
    
    public function callAPI($method, $url, $data) {
        $curl = curl_init();
     
        switch ($method){
           case "POST":
              curl_setopt($curl, CURLOPT_POST, 1);
              if ($data)
                 curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
              break;
           case "PUT":
              curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
              if ($data)
                 curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
              break;
           default:
              if ($data)
                 $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
     }
}
