<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Shipping;

class ShippingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {   
        $data =  DB::table('shipping')->where('user_id', Auth::user()->id )->first();
        return $data;
    }

    public function update()
    {
        $id = Auth::user()->id;

        $this->validate(request(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'adress' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);

        $shipping = DB::table('shipping')->where('user_id', $id )->first();
        
        if (empty($shipping))
        {
            DB::table('shipping')
                ->insert([
                    'user_id' => $id,
                    'first_name' => request('first_name'),
                    'last_name' => request('last_name'),
                    'adress' => request('adress'), 
                    'postal_code' => request('postal_code'),
                    'city' => request('city'),
                    'country' => request('country')
                ]);
        } else {
            DB::table('shipping')
                ->where('user_id', $id)
                ->update([
                    'first_name' => request('first_name'),
                    'last_name' => request('last_name'),
                    'adress' => request('adress'), 
                    'postal_code' => request('postal_code'),
                    'city' => request('city'),
                    'country' => request('country')  
                ]);
        }

        return back();
           
    }
}

