<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\ProductController;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {   
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }

    public function update(User $user)
    { 
        if (Auth::user()->id != $user->id) {
            if (Auth::user()->role_id != 1) {
                return redirect()->back();    
            }
        }

        $this->validate(request(), [
            'name' => 'required',
            'password' => '',
            'currency' => 'required'
        ]);
        /*
        if (request('admin') != null) {
            $user->role_id = 1;
        } else {
            $user->role_id = 2;
        }
        */
        $user->name = request('name');
        $user->currency = request('currency');
        
        date_default_timezone_set("Europe/Amsterdam"); 
        $user->updated_at = time();
        if (!empty(request('password'))) { 
            $user->password = bcrypt(request('password'));
        } else {
            $user->password = $user->password;
        }

        $user->save();

        return back();

    }

    public function getProfile() {
        $orders = Auth::user()->orders;
        $orders->transform(function($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });

        return view('profile', ['orders' => $orders]);
    }
    
}

