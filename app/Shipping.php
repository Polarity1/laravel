<?php

namespace App;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model {    
    protected $table = 'shipping';
    protected $fillable = ['user_id', 'first_name', 'last_name', 'adress', 'postal_code', 'city', 'country']; 
}
