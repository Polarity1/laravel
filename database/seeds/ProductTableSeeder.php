<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $product = new \App\Product([
            'imagePath' => 'https://www.businezz.nl/media/10/9789000348640.jpg',
            'title' => 'Steve Jobs - Autobiography',
            'description' => 'Autobiography of Steve Jobs and how he created his Apple imperium',
            'price' => '21.99'
        ]);
        $product->save();

    }
}
